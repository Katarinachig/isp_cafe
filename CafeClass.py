import Man


class CafeClass:
    class SingleCafe:

        menu = []
        last_prices = []
        discount_done = False

        def __init__(self, dish):
            if dish is not None:
                self.menu.append(dish)
                self.last_prices.append(dish.price)

        def get(self):
            return self.menu

        def add(self, dish):
            if not(dish in self.menu):
                self.menu.append(dish)
                self.last_prices.append(dish.price)

        def remove(self, name):
            dish = self.findByName(name)
            if dish in self.menu:
                self.menu.remove(dish)
                self.last_prices.remove(dish.price)

        def findByName(self, name):
            for dish in self.menu:
                if dish.name == name:
                    return dish
            raise Exception("no that dish in menu")

        def change(self, dish):
            previous_dish = self.find_dish(dish)
            if previous_dish is not None:
                self.menu.remove(previous_dish)
                self.last_prices.remove(previous_dish.price)
                self.menu.append(dish)
                self.last_prices.append(dish.price)
            else:
                raise Exception("we do not have that dish")

        def do_discounts(self, proc):
           # if not self.discount_done:
            for l in self.menu:
                l.price = proc * l.price/100
            #self.discount_done = True
            #else:
            #    raise Exception("you have already done them")

        def return_prices(self):
            #if self.discount_done:
            counter = 0
            for l in self.menu:
                l.price = self.last_prices[counter]
                counter += 1
            #self.discount_done = False
            #else:
             #   raise Exception("you have no done discounts")

        def find_dish(self, dish):
            name = dish.name
            for temp_dish in self.menu:
                if name == temp_dish.name:
                    return temp_dish
            return None

        def return_group(self, group):
            enter_list = []
            for i in self.menu:
                if i.group == group:
                    enter_list.append((i.name, str(i.price), str(i.energy)))
            return enter_list


    inst = None

    def __init__(self, dish=None):
        if not self.inst:
            self.inst = self.SingleCafe(dish)
        else:
            raise Exception("you have already had cafe")

    def get(self):
        return self.inst.get()

    def add(self, customer, dish):
        if customer.role == "admin":
            if not self.inst:
                CafeClass.inst = CafeClass.SingleCafe(dish)
            self.inst.add(dish)

    def remove(self, customer, name):
        if customer.role == "admin":
            self.inst.remove(name)

    def change(self, customer, dish):
        if customer.role == "admin":
            if not self.inst:
                CafeClass.inst = CafeClass.SingleCafe(dish)
            self.inst.change(dish)

    def do_discounts(self, customer, proc):
        if customer.role == "admin":
            if not self.inst:
                raise Exception("no cafe")
            if (proc <= 0) or (proc >= 100):
                raise Exception("incorrect percent")
            self.inst.do_discounts(proc)

    def return_prices(self,  customer):
        if customer.role == "admin":
            if not self.inst:
                raise Exception("no cafe")
            self.inst.return_prices()

    def find(self, dish):
        if not self.inst:
            raise Exception("no cafe")
        return self.inst.find_dish(dish)

    def return_group(self, index):
        if not self.inst:
            raise Exception("no cafe")
        else:
            if index == 0:
                group = "soups"
            elif index == 1:
                group = "garnish"
            elif index == 2:
                group = "desert"
            else:
                group = "drinks"
            return self.inst.return_group(group)

    def get_menu(self):
        if not self.inst:
            raise Exception("no cafe")
        else:
            return self.inst.menu

    def name_find(self, name):
        if not self.inst:
            raise Exception("no cafe")
        return self.inst.findByName(name)
