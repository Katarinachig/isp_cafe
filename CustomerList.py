import Man

class CustomerList:

    customers = []

    def add(self, customer):
        self.customers.append(customer)

    def authorisation(self, name, password):
        for i in self.customers:
            if i.name == name:
                if i.password == password:
                    return i
        return None

    def find(self, name):
        for customer in self.customers:
            if customer.name == name:
                return customer
        return None

    def return_users(self, user):
        result = [i.name for i in self.customers if i.name != user.name]
        return result

    def remove(self, user):
        self.customers.remove(user)

