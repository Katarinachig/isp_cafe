class Dish:
    group = None
    name = None
    price = 0
    energy = 0

    def __init__(self, group, name, price, energy):
        self.group = group
        self.name = name
        self.price = price
        self.energy = energy
