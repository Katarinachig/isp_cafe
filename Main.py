import CafeClass
import CustomerList
import Man
import Dish
from authorisation import Ui_MainWindow  # импорт нашего сгенерированного файла
from dishes import Ui_DishWindow
from bill import Ui_BillWindow
from for_admin import Ui_AdminWindow
from percent import Ui_PercenrWindow
from remove_dish import Ui_RemoveWindow
from new_dish import Ui_New_dishWindow
from offer import Ui_OfferWindow
from PyQt5 import QtGui, QtWidgets, QtCore
import sys
import pymongo


cl = CustomerList.CustomerList()
conn = pymongo.MongoClient()
db = conn.local
menu_db = db.Menu
cafe = CafeClass.CafeClass()
for dish in menu_db.find():
    cafe.add(Man.Customer("Admin", "admin"), Dish.Dish(dish["group"], dish["name"], dish["price"], dish["energy"]))
user_db = db.Customers
for man in user_db.find():
    cl.add(Man.Customer(man["name"], man["password"], man["role"], man["previous_orders"]))
user = None


class authorisationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(authorisationWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.secondWin = None
        self.admin_window = None
        self.ui.setupUi(self)
        self.ui.Sign_in.clicked.connect(self.Sign_in)
        self.ui.Registrate.clicked.connect(self.Registrate)
        self.ui.password.setEchoMode(QtWidgets.QLineEdit.Password)

    def Sign_in(self):
        user = None
        while user is None:
            name = self.ui.login.text()
            password = self.ui.password.text()
            user = cl.authorisation(name, password)
            self.ui.login.clear()
            self.ui.password.clear()
        if user.role == "user" and not self.secondWin:
            self.secondWin = DishWindow(self, user)
            self.secondWin.show()
        elif user.role == "admin" and not self.admin_window:
            self.admin_window = AdminWindow(self, user)
            self.admin_window.show()

    def Registrate(self):
        user = None
        while user is None:
            name = self.ui.login.text()
            password = self.ui.password.text()
            if name == "Admin":
                self.ui.login.setStyleSheet("background-color: rgb(255, 0, 0);")
                self.ui.login.clear()
                user = None
            else:
                user = Man.Customer(name, password)
                cl.add(user)
                doc = {"name": name, "password": password, "role": "user", "previous_orders": []}
                user_db.save(doc)
                self.ui.login.clear()
                self.ui.password.clear()
            if not self.secondWin:
                self.secondWin = DishWindow(self, user)
                self.secondWin.show()


class DishWindow(QtWidgets.QMainWindow):
    user = None

    def __init__(self, parent=None, user=None):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.billWindow = None
        self.offerWindow = None
        self.ui = Ui_DishWindow()
        self.ui.setupUi(self)
        self.ui.setupUi(self)
        self.ui.comboBox.addItem("soups")
        self.ui.comboBox.addItem("garnish")
        self.ui.comboBox.addItem("desert")
        self.ui.comboBox.addItem("drinks")
        header = self.ui.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        self.Display()
        self.ui.comboBox.currentIndexChanged['int'].connect(self.Display)
        self.ui.pushButton.clicked.connect(self.AddToChoice)
        self.ui.pushButton_2.clicked.connect(self.Pay)
        self.ui.pushButton_3.clicked.connect(self.PreviousOrder)

    def PreviousOrder(self):
        dish_list, bill = self.user.repeat_order(cafe)
        if len(dish_list) != 0:
            if not self.billWindow:
                self.billWindow = BillWindow(self, self.user, dish_list, bill)
                self.billWindow.show()
        else:
            self.Close()


    def Display(self):
        self.ui.tableWidget.clear()
        index = self.ui.comboBox.currentIndex()
        result_list = cafe.return_group(index)
        self.ui.tableWidget.setRowCount(len(result_list))
        self.ui.tableWidget.setColumnCount(3)
        self.ui.tableWidget.setHorizontalHeaderLabels(('name', 'price', "energy"))
        if len(result_list) != 0:
            row = 0
            for tup in result_list:
                col = 0
                for item in tup:
                    cellinfo = QtWidgets.QTableWidgetItem(item)
                    cellinfo.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                    self.ui.tableWidget.setItem(row, col, cellinfo)
                    col += 1
                row += 1

    def AddToChoice(self):
        if self.user.add_counter == 0:
            self.user.previous_order.clear()
        if self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 0) is not None:
            item_name = self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 0).text()
            item_price = float(self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 1).text())
            self.user.previous_order.append(cafe.name_find(item_name))
            self.user.AddToPrevious(item_name)
            self.user.add_counter += 1


    def Pay(self):
        if not self.offerWindow:
            self.offerWindow = OfferWindow(self, self.user)
            self.offerWindow.show()
            user_db.remove({})
            for i in cl.customers:
                doc = {"name": i.name, "password": i.password, "role": i.role, "previous_orders": i.previous_orders}
                user_db.save(doc)
            self.close()

    def closeEvent(self, event):
        self.parent.secondWin = None
        if not self.offerWindow:
            self.offerWindow = OfferWindow(self, self.user)
            self.offerWindow.show()

    def Close(self):
        self.parent.secondWin = None
        if not self.offerWindow:
            self.offerWindow = OfferWindow(self, self.user)
            self.offerWindow.show()
        self.close()


class OfferWindow(QtWidgets.QMainWindow):
    user = None

    def __init__(self, parent=None, user=None):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.billWindow = None
        self.ui = Ui_OfferWindow()
        self.ui.setupUi(self)
        result_list = self.user.giveAdvance()
        this_order = [i.name for i in self.user.previous_order]
        self.ui.tableWidget.clear()
        header = self.ui.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        self.ui.tableWidget.setRowCount(len(list(set(result_list) - set(this_order))))
        self.ui.tableWidget.setColumnCount(2)
        self.ui.tableWidget.setHorizontalHeaderLabels(('name', 'price'))
        if len(list(set(result_list) - set(this_order))) != 0:
            row = 0
            for name in result_list:
                dish = cafe.name_find(name)
                if dish.name not in this_order:
                    cellinfo = QtWidgets.QTableWidgetItem(dish.name)
                    cellinfo.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                    self.ui.tableWidget.setItem(row, 0, cellinfo)
                    cellinfo = QtWidgets.QTableWidgetItem(dish.price)
                    self.ui.tableWidget.setItem(row, 1, cellinfo)
                    row += 1
        self.ui.AddButton.clicked.connect(self.Add)

    def Add(self):
        if self.user.add_counter == 0:
            self.user.previous_order.clear()
        if self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 0) is not None:
            item_name = self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 0).text()
            #item_price = float(self.ui.tableWidget.item(self.ui.tableWidget.currentRow(), 1).text())
            self.user.previous_order.append(cafe.name_find(item_name))
            self.user.AddToPrevious(item_name)
            self.user.add_counter += 1

    def closeEvent(self, event):
        self.parent.offerWindow = None
        if not self.billWindow:
            self.billWindow = BillWindow(self, self.user)
            self.billWindow.show()


class BillWindow(QtWidgets.QMainWindow):
    user = None
    pay_list = None
    bill = 0

    def __init__(self, parent=None, user=None, pay_list=None, bill = 0):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.pay_list = pay_list
        self.bill = bill
        self.ui = Ui_BillWindow()
        self.ui.setupUi(self)
        self.ui.setupUi(self)
        if pay_list is None:
            bill = 0
            for i in user.previous_order:
                self.ui.textBrowser.append(str(str(i.name)) + "                " + str(i.price))
                bill += float(i.price)
            self.ui.textBrowser.append("price for all is" + "   " + str(bill))
        else:
            for i in self.pay_list:
                self.ui.textBrowser.append(str(i.name) + "                " + str(i.price))
            self.ui.textBrowser.append("price for all is" + "   " + str(self.bill))

    def closeEvent(self, event):
        self.parent.billWindow = None


class AdminWindow(QtWidgets.QMainWindow):
    user = None
    diskounts_done = False

    def __init__(self, parent=None, user=None):
        super().__init__(parent)
        self.DiskountWindow = None
        self.NewDishWindow = None
        self.RemoveWindow = None
        self.secondWin = None
        self.parent = parent
        self.user = user
        self.ui = Ui_AdminWindow()
        self.ui.setupUi(self)
        self.ui.setupUi(self)
        for i in cl.return_users(self.user):
            self.ui.comboBox.addItem(i)
        self.ui.add_admin.clicked.connect(self.AddToAdmins)
        self.ui.remove_admin.clicked.connect(self.RemoveFromAdmins)
        self.ui.do_discounts.clicked.connect(self.Diskount)
        self.ui.retrn_prices.clicked.connect(self.ReturnPrices)
        self.ui.new_dish.clicked.connect(self.AddDish)
        self.ui.change_dish.clicked.connect(self.ChangeDish)
        self.ui.remove_dish.clicked.connect(self.Remove)
        self.ui.show_dishes.clicked.connect(self.Show)

    def Show(self):
        if not self.secondWin:
            self.secondWin = DishWindow(self, self.user)
            self.secondWin.show()

    def Remove(self):
        if not self.RemoveWindow:
            self.RemoveWindow = RemoveWindow(self, self.user)
        self.RemoveWindow.show()

    def ChangeDish(self):
        if not self.NewDishWindow:
            self.NewDishWindow = NewDishWindow(self, self.user, "change")
        self.NewDishWindow.show()

    def AddDish(self):
        if not self.NewDishWindow:
            self.NewDishWindow = NewDishWindow(self, self.user)
        self.NewDishWindow.show()


    def AddToAdmins(self):
        name = self.ui.comboBox.currentText()
        temp_user = cl.find(name)
        if temp_user.role == "admin":
            self.ui.result.append(str(temp_user.name) + " is already admin")
        else:
            cl.remove(temp_user)
            cl.add(Man.Customer(temp_user.name, temp_user.password, "admin"))
            user_db.update({"name": temp_user.name, "password": temp_user.password}, {"$set": {"role": "admin"}})
            self.ui.result.append(str(temp_user.name) + " is admin now")

    def RemoveFromAdmins(self):
        name = self.ui.comboBox.currentText()
        temp_user = cl.find(name)
        if temp_user.role == "user":
            self.ui.result.append(str(temp_user.name) + " is already user")
        else:
            cl.remove(temp_user)
            cl.add(Man.Customer(temp_user.name, temp_user.password, "user"))
            user_db.update({"name": temp_user.name, "password": temp_user.password}, {"$set": {"role": "user"}})
            self.ui.result.append(str(temp_user.name) + " is user now")

    def closeEvent(self, event):
        self.parent.admin_window = None

    def Diskount(self):
        if not self.diskounts_done:
            if not self.DiskountWindow:
                self.DiskountWindow = DiskountWindow(self, self.user)
            self.DiskountWindow.show()
        else:
            self.ui.result.append("we have already done discounts")

    def ReturnPrices(self):
        if self.diskounts_done:
            cafe.return_prices(self.user)
            self.ui.result.append("prices are returned")
            self.diskounts_done = False
        else:
            self.ui.result.append("we have not done discounts")


class DiskountWindow(QtWidgets.QMainWindow):
    user = None

    def __init__(self, parent=None, user=None):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.ui = Ui_PercenrWindow()
        self.ui.setupUi(self)
        self.ui.OkButton.clicked.connect(self.Diskount)

    def Diskount(self):
        if len(self.ui.procentTextEdit.text()) != 0:
            try:
                percent = float(self.ui.procentTextEdit.text())
                cafe.do_discounts(self.user, percent)
                self.parent.diskounts_done = True
                self.parent.ui.result.append("disounts done")
                self.close()
            except Exception:
                self.parent.ui.result.append("you have entered incorrect data")
                self.close()

    def closeEvent(self, event):
        self.parent.DiskountWindow = None


class NewDishWindow (QtWidgets.QMainWindow):
    user = None

    def __init__(self, parent=None, user=None, function="add"):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.function = function
        self.ui = Ui_New_dishWindow()
        self.ui.setupUi(self)
        self.ui.cancelButton.clicked.connect(self.Cancel)
        self.ui.OkButton.clicked.connect(self.Add)

    def closeEvent(self, event):
        self.parent.NewDishWindow = None

    def Cancel(self):
        self.parent.NewDishWindow = None
        self.close()

    def Add(self):
        try:
            name = self.ui.nameEdit.text()
            price = float(self.ui.priceEdit.text())
            energy = float(self.ui.energyEdit.text())
            group = self.ui.lineEdit.text()
            if price <= 0 or energy <=0 or group not in ["soups", "garnish", "desert", "drinks"] or len(name) * len(self.ui.priceEdit.text()) * len(self.ui.energyEdit.text()) * len(group) == 0:
                self.parent.ui.result.append("You have entered incorrect data")
                self.Cancel()
            else:
                    if self.function == "add":
                        cafe.add(self.user, Dish.Dish(group, name, price, energy))
                        doc = {"name": name, "group": group, "price": price, "energy": energy}
                        menu_db.save(doc)
                        self.parent.ui.result.append("new dish is added")
                        self.Cancel()
                    else:
                        cafe.change(self.user, Dish.Dish(group, name, price, energy))
                        menu_db.remove({})
                        for i in cafe.get_menu():
                            doc = {"name": i.name, "group": i.group, "price": i.price, "energy": i.energy}
                            menu_db.save(doc)
                        self.parent.ui.result.append("dish is changed")
                        self.Cancel()
        except Exception:
            self.parent.ui.result.append("You have no entered data try again")
            self.ui.nameEdit.clear()
            self.ui.priceEdit.clear()
            self.ui.energyEdit.clear()
            self.ui.lineEdit.clear()


class RemoveWindow (QtWidgets.QMainWindow):
    user = None

    def __init__(self, parent=None, user=None):
        super().__init__(parent)
        self.parent = parent
        self.user = user
        self.ui = Ui_RemoveWindow()
        self.ui.setupUi(self)
        self.ui.cancelButton_2.clicked.connect(self.Cancel)
        self.ui.okButton.clicked.connect(self.Remove)

    def closeEvent(self, event):
        self.parent.RemoveWindow = None

    def Cancel(self):
        self.parent.RemoveWindow = None
        self.close()

    def Remove(self):
        if len(self.ui.nameEdit.text()) != 0:
            name = self.ui.nameEdit.text()
            cafe.remove(self.user, name)
            menu_db.remove({"name": name})
            self.parent.ui.result.append(name + " is removed")
            self.Cancel()


app = QtWidgets.QApplication([])
application = authorisationWindow()
application.show()
sys.exit(app.exec())
