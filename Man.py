import CafeClass
import CustomerList
import pickle
from collections import Counter


class Customer:
        password = None
        name = None
        previous_order = []
        previous_orders = []
        role = "user"
        add_counter = 0

        def __init__(self, name=None, password=None, role=None, previous_orders = []):
            self.password = password
            self.name = name
            self.previous_orders = previous_orders
            if role is None:
                if name == "Admin":
                    self.role = "admin"
            elif role == "admin":
                self.role = "admin"

        def AddToPrevious(self, name):
            self.previous_orders.append(name)

        def giveAdvance(self):
            counted = dict(Counter(self.previous_orders))
            rez = list(counted.values())
            l = len(rez)
            if l < 10:
                while l < 10:
                    rez.append(0)
                    l += 1
            with open(r'/home/kate/univer/sem4/isp/cafe/ml.pickle', 'rb') as f:
                ml = pickle.load(f)
            features = ml.transform([rez, [0]*10])
            rez = features[0]
            for_return = []
            for i in rez:
                a, counted = self.find_in_previous(i, counted)
                if a is None:
                    break
                for_return.append(a)
            return for_return

        def find_in_previous(self, number, counted):
            if len(counted) != 0:
                keys = counted.keys()
                for i in keys:
                    if counted[i] == number:
                        del counted[i]
                        return i, counted
            return None, None

        '''
        def make_order(self, cafe):
                bill = 0
                self.previous_order = []
                exit_code = int(input("To change user enter 0"))
                while exit_code != 0:
                        gr = int(input("group"))
                        dish = int(input("dish"))
                        if len(list(cafe.get().keys())) < gr:
                                i = list(cafe.get().keys())[gr - 1]
                                if len(list(cafe.get()[i].keys())) < dish:
                                        j = list(cafe.get()[i].keys())[dish - 1]
                                        bill += cafe.get()[i][j]
                                        self.previous_order.append(j)
                        exit_code = int(input("To change user enter 0"))
                return self.bill
        '''

        def repeat_order(self, cafe):
            bill = 0
            for i in self.previous_order:
                bill += cafe.find(i).price
            return self.previous_order, bill

        def add_to_admins(self, name, list_of_customers):
            if (self.role == "admin") and (list_of_customers.find(name) is not None):
                customer = list_of_customers.find(name)
                list_of_customers.customers.remove(customer)
                customer.role = "admin"
                list_of_customers.customers.add(customer)