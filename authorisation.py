# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'authorisation.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        MainWindow.setAutoFillBackground(True)
        MainWindow.resize(339, 476)
        MainWindow.setMaximumSize(339, 476)
        MainWindow.setMinimumSize(339, 476)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 331, 191))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.login = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.login.setObjectName("login")
        self.verticalLayout.addWidget(self.login)
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.password = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.password.setObjectName("password")
        self.verticalLayout.addWidget(self.password)
        self.Sign_in = QtWidgets.QPushButton(self.centralwidget)
        self.Sign_in.setGeometry(QtCore.QRect(0, 350, 331, 25))
        self.Sign_in.setObjectName("Sign_in")
        self.Registrate = QtWidgets.QPushButton(self.centralwidget)
        self.Registrate.setGeometry(QtCore.QRect(0, 390, 331, 25))
        self.Registrate.setObjectName("Registrate")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.Registrate.clicked.connect(self.login.selectAll)
        self.Registrate.clicked.connect(self.password.selectAll)
        self.Sign_in.clicked.connect(self.login.selectAll)
        self.Sign_in.clicked.connect(self.password.selectAll)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Enter login"))
        self.label_2.setText(_translate("MainWindow", "Enter password"))
        self.Sign_in.setText(_translate("MainWindow", "Sign in"))
        self.Registrate.setText(_translate("MainWindow", "Registrate"))


