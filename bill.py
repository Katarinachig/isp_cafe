# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bill.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BillWindow(object):
    def setupUi(self, BillWindow):
        BillWindow.setObjectName("BillWindow")
        BillWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        BillWindow.setAutoFillBackground(True)
        BillWindow.resize(340, 474)
        BillWindow.setMaximumSize(340, 474)
        BillWindow.setMinimumSize(340, 474)
        self.centralwidget = QtWidgets.QWidget(BillWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 331, 451))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textBrowser = QtWidgets.QTextBrowser(self.verticalLayoutWidget)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        BillWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(BillWindow)
        self.statusbar.setObjectName("statusbar")
        BillWindow.setStatusBar(self.statusbar)

        self.retranslateUi(BillWindow)
        QtCore.QMetaObject.connectSlotsByName(BillWindow)

    def retranslateUi(self, BillWindow):
        _translate = QtCore.QCoreApplication.translate
        BillWindow.setWindowTitle(_translate("BillWindow", "MainWindow"))


