# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'for_admin.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AdminWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        MainWindow.setAutoFillBackground(True)
        MainWindow.resize(343, 474)
        MainWindow.setMaximumSize(343, 474)
        MainWindow.setMinimumSize(343, 474)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(0, 60, 341, 25))
        self.comboBox.setObjectName("comboBox")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 10, 331, 17))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.add_admin = QtWidgets.QPushButton(self.centralwidget)
        self.add_admin.setGeometry(QtCore.QRect(0, 130, 331, 25))
        self.add_admin.setObjectName("add_admin")
        self.remove_admin = QtWidgets.QPushButton(self.centralwidget)
        self.remove_admin.setGeometry(QtCore.QRect(0, 170, 331, 25))
        self.remove_admin.setObjectName("remove_admin")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(0, 230, 331, 17))
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.new_dish = QtWidgets.QRadioButton(self.centralwidget)
        self.new_dish.setGeometry(QtCore.QRect(10, 270, 112, 23))
        self.new_dish.setObjectName("new_dish")
        self.remove_dish = QtWidgets.QRadioButton(self.centralwidget)
        self.remove_dish.setGeometry(QtCore.QRect(10, 300, 112, 23))
        self.remove_dish.setObjectName("remove_dish")
        self.change_dish = QtWidgets.QRadioButton(self.centralwidget)
        self.change_dish.setGeometry(QtCore.QRect(10, 330, 112, 23))
        self.change_dish.setObjectName("change_dish")
        self.do_discounts = QtWidgets.QRadioButton(self.centralwidget)
        self.do_discounts.setGeometry(QtCore.QRect(180, 270, 112, 23))
        self.do_discounts.setObjectName("do_discounts")
        self.retrn_prices = QtWidgets.QRadioButton(self.centralwidget)
        self.retrn_prices.setGeometry(QtCore.QRect(180, 300, 112, 23))
        self.retrn_prices.setObjectName("retrn_prices")
        self.show_dishes = QtWidgets.QRadioButton(self.centralwidget)
        self.show_dishes.setGeometry(QtCore.QRect(180, 330, 112, 23))
        self.show_dishes.setObjectName("show_dishes")
        self.result = QtWidgets.QTextBrowser(self.centralwidget)
        self.result.setGeometry(QtCore.QRect(0, 370, 341, 71))
        self.result.setObjectName("result")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(0, 210, 341, 16))
        self.line.setFrameShadow(QtWidgets.QFrame.Plain)
        self.line.setLineWidth(2)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setObjectName("line")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        #self.show_dishes.clicked.connect(MainWindow.slot1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Work with users"))
        self.add_admin.setText(_translate("MainWindow", "Add to admins"))
        self.remove_admin.setText(_translate("MainWindow", "Remove from admins"))
        self.label_2.setText(_translate("MainWindow", "Work with cafe"))
        self.new_dish.setText(_translate("MainWindow", "Add new dish"))
        self.remove_dish.setText(_translate("MainWindow", "Remove dish"))
        self.change_dish.setText(_translate("MainWindow", "Change dish"))
        self.do_discounts.setText(_translate("MainWindow", "Do diskounts"))
        self.retrn_prices.setText(_translate("MainWindow", "Return prices"))
        self.show_dishes.setText(_translate("MainWindow", "show_dishes"))


