# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_dish.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_New_dishWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        MainWindow.setAutoFillBackground(True)
        MainWindow.resize(415, 244)
        MainWindow.setMaximumSize(415, 244)
        MainWindow.setMinimumSize(415, 244)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 0, 401, 17))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(320, 70, 81, 21))
        self.label_2.setObjectName("label_2")
        self.nameEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.nameEdit.setGeometry(QtCore.QRect(0, 70, 311, 25))
        self.nameEdit.setObjectName("nameEdit")
        self.priceEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.priceEdit.setGeometry(QtCore.QRect(0, 110, 311, 25))
        self.priceEdit.setObjectName("priceEdit")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(320, 110, 81, 21))
        self.label_3.setObjectName("label_3")
        self.energyEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.energyEdit.setGeometry(QtCore.QRect(0, 150, 311, 25))
        self.energyEdit.setText("")
        self.energyEdit.setObjectName("energyEdit")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(320, 150, 91, 21))
        self.label_4.setObjectName("label_4")
        self.OkButton = QtWidgets.QPushButton(self.centralwidget)
        self.OkButton.setGeometry(QtCore.QRect(340, 200, 71, 21))
        self.OkButton.setObjectName("OkButton")
        self.cancelButton = QtWidgets.QPushButton(self.centralwidget)
        self.cancelButton.setGeometry(QtCore.QRect(0, 200, 71, 21))
        self.cancelButton.setObjectName("cancelButton")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(0, 30, 311, 25))
        self.lineEdit.setObjectName("lineEdit")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(320, 30, 81, 17))
        self.label_5.setObjectName("label_5")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Enter data for dish"))
        self.label_2.setText(_translate("MainWindow", "Enter name"))
        self.label_3.setText(_translate("MainWindow", "Enter price"))
        self.label_4.setText(_translate("MainWindow", "Enter energy"))
        self.OkButton.setText(_translate("MainWindow", "OK"))
        self.cancelButton.setText(_translate("MainWindow", "CANCEL"))
        self.label_5.setText(_translate("MainWindow", "Enter group"))


