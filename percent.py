# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'procent.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PercenrWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        MainWindow.setAutoFillBackground(True)
        MainWindow.resize(255, 146)
        MainWindow.setMaximumSize(255, 146)
        MainWindow.setMinimumSize(255, 146)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 10, 241, 17))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.procentTextEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.procentTextEdit.setGeometry(QtCore.QRect(0, 50, 211, 31))
        self.procentTextEdit.setObjectName("procentTextEdit")
        self.OkButton = QtWidgets.QPushButton(self.centralwidget)
        self.OkButton.setGeometry(QtCore.QRect(210, 50, 41, 31))
        self.OkButton.setObjectName("OkButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Enter percent of discount"))
        self.OkButton.setText(_translate("MainWindow", "OK"))


