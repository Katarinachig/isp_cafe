# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'remove_dish.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RemoveWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setStyleSheet(open("FirstStyle.qss", "r").read())
        MainWindow.setAutoFillBackground(True)
        MainWindow.resize(278, 150)
        MainWindow.setMaximumSize(278, 150)
        MainWindow.setMinimumSize(278, 150)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 10, 261, 17))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.nameEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.nameEdit.setGeometry(QtCore.QRect(0, 50, 271, 25))
        self.nameEdit.setObjectName("nameEdit")
        self.okButton = QtWidgets.QPushButton(self.centralwidget)
        self.okButton.setGeometry(QtCore.QRect(210, 100, 61, 25))
        self.okButton.setObjectName("okButton")
        self.cancelButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.cancelButton_2.setGeometry(QtCore.QRect(0, 100, 61, 25))
        self.cancelButton_2.setObjectName("cancelButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "enter name of dish"))
        self.okButton.setText(_translate("MainWindow", "OK"))
        self.cancelButton_2.setText(_translate("MainWindow", "CANCEL"))


